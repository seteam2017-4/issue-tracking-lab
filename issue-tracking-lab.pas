Program issue_tracking_lab;

type SizeOfFiles=record
     Mbs: integer;
     Kbs: integer;
end; 

var Mas: array of SizeOfFiles; Value, Size, TotalSize, err, maxx: integer; Quote: string; Flag, LetterFlag: boolean;

Procedure GenerateListOfSizes(var A: array of SizeOfFiles; N, Sum: integer);
  var i:byte; SumGbs, SumMbs, SumKbs: integer;
    begin
      for i := 0 to N-1 do
      begin
        writeln('File number ', i+1);
        with A[i] do
             begin
                  Mbs := random(300);
                  SumMbs:=SumMbs+Mbs;
                  write(Mbs, ' M ');
                  Kbs := random(1023);
                  SumKbs:=SumKbs+Kbs;
                  writeln(Kbs, ' K ');
             end;
        writeln;
      end;
      if (SumKbs>1023) then
        begin
             SumMbs:=SumMbs+(SumKbs div 1024);
             SumKbs:=SumKbs mod 1024;
        end;
      if (SumMbs>1023) then
        begin
             SumGbs:=SumMbs div 1024;
             SumMbs:=SumMbs mod 1024;
        end;
      Sum:=SumGbs*1024*1024+SumMbs*1024+SumKbs;
      Writeln('Total size of all files:');
      writeln(SumGbs, 'G ', SumMbs, 'M ', SumKbs, 'K');
      writeln;
   end;
	
Procedure FindMinMaxAverage(const A:array of SizeOfFiles;const N: integer);
  var i:byte;min,max,mid:real;k:array[1..10] of integer;
    Begin
    min:=0;
    max:=0;
    mid:=0;
    for i:=1 to 10 do
    begin
    k[i]:=A[i].Kbs + A[i].Mbs * 1024;
    end;
      for i:=1 to 10 do
      begin
        if k[i] > max then
        max:=k[i];
        if k[i] < min then
        min:=k[i];
      end;
    mid:=(max+min)/2;
    writeln(' Max = ',max,' Min = ',min,' Mid = ',mid);
    End;

Begin
randomize;
maxx:=100;
setlength(mas,maxx) ;
Size:=10;
GenerateListOfSizes(Mas, Size, TotalSize);
FindMinMaxAverage(Mas, Size);
writeln('Enter quote(number of Mbs or Gbs with suffix M or G)');
while Flag=false do
   begin
       readln(Quote);
       if (pos('M', Quote)=0)and(pos('G', Quote)=0) then
           begin
            writeln('Enter correct quote');
            Flag:=false;
           end
       else
            Flag:=true;
    end;

if (pos('M', Quote)<>0) then
    begin
         delete(Quote, pos('M', Quote), 1);
         LetterFlag:=False;
    end;
if (pos('G', Quote)<>0) then
    begin
         delete(Quote, pos('G', Quote), 1);
         LetterFlag:=True;
    end;
val(Quote, Value,err);
if LetterFlag=False then
    Value:=Value*1024
else
    Value:=Value*1024*1024;
If ((Value-TotalSize)<0) then
     writeln('Yes')
Else
     writeln('No');
   
readln;
End.